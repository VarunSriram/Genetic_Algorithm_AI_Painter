
public class FitnessCalculator {
	static byte[] solution = new byte[64];
	
	public static void setSolution(byte[] sol){
		solution = sol;
	}
	static void setSolution(String newSolution) {
        solution = new byte[newSolution.length()];
        // Loop through each character of our string and save it in our byte 
        // array
        for (int i = 0; i < newSolution.length(); i++) {
            String character = newSolution.substring(i, i + 1);
            if (character.contains("0") || character.contains("1")) {
                solution[i] = Byte.parseByte(character);
            } else {
                solution[i] = 0;
            }
        }
	}
	static int getFitness(Individual ind){
		int fitness = 0;
		
		for(int i =0; i < ind.getSize() && i< solution.length; i++ ){
			if(ind.getGene(i) == solution[i]){
				fitness++;
			}
		}
		
		return fitness;
	}
	
	static int getMaxFitness(){
		int maxFitness = solution.length;
		return maxFitness;
	}
}
