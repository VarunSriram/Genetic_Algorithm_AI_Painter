import java.util.ArrayList;
import java.util.Arrays;

public class PixelPerfect extends RunGene {
	private EvolutionArchiver ea = new EvolutionArchiver();
	public PixelPerfect(String gene) {
		super(gene);
		
	}

	@Override
	public void runGA() {
		
		ArrayList<String> pixels = new ArrayList<String>(Arrays.asList(gene.split("\n")));
		
		int i = 0;
		for(String s : pixels){
			Individual.setDefaultGeneLength(s.length());
		 	FitnessCalculator.setSolution(s);
		 	Population myPop = new Population(250, true);
		 	int generationCount = 0;
		 	
		 	 while (myPop.getFittest().getFitness() < FitnessCalculator.getMaxFitness()) {
		            generationCount++;
		            System.out.println("Generation: " + generationCount + " Fittest: " + myPop.getFittest().getFitness());
		            ea.addEntry(i, myPop.getFittest().toString());
		            myPop = GeneticAlgorithm.evolvePopulation(myPop);
		    }
			
		 	System.out.println("Solution found!");
	        System.out.println("Generation: " + generationCount);
	        System.out.println("Genes:");
	        System.out.println(myPop.getFittest());
	        ea.addEntry(i, myPop.getFittest().toString());
			i++;
		
		}
		System.out.println("Size of Pixels: "+pixels.size());
		this.outputResult();
	}
	
	public void outputResult(){
		int genSize = ea.padData();
		System.out.println("List Size: "+genSize);
		for(int i=0;i<genSize;i++){
			String x = ImageEncoder.getInstance().getEncodingFormate(ea.getGeneration(i));
			ImageEncoder.getInstance().decodeImage(x, "ImageSource/"+i+"final.jpg");
			
			
		}
		
	}
	
	

}
