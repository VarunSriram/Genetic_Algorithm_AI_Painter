import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.imageio.ImageIO;
import java.awt.image.DataBufferInt;
public class ImageEncoder {
	private static ImageEncoder instance;
	
	protected ImageEncoder(){
		
	}
	
	
	public static ImageEncoder getInstance(){
		if(instance == null){
			instance = new ImageEncoder();
		}
		return instance;
	}
	
	public String encodeImage(String fileDir){
		System.out.println("Encoding Image");
		BufferedImage in = null;
		BufferedImage newImage = null;
		String bitEncode = "";
		try {
		    in = ImageIO.read(new File(fileDir));
		    newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_RGB);

		    Graphics2D g = newImage.createGraphics();
		    g.drawImage(in, 0, 0, in.getWidth(), in.getHeight(), null);
		    g.dispose();
	
		} catch (IOException e) {
		}
		
		System.out.println("Loading image");
		int x = newImage.getWidth();
		int y = newImage.getHeight();
		System.out.println("Width: "+ x + " Height: "+y);
		int[] pixels = ((DataBufferInt)newImage.getRaster().getDataBuffer()).getData();
		for(Integer i: pixels){
			Color c = new Color(i);
			byte br=(byte) c.getRed();
			byte bg=(byte) c.getGreen();
			byte bb =(byte) c.getBlue();
			String sr = String.format("%8s", Integer.toBinaryString(br & 0xFF)).replace(' ', '0');
			String sg = String.format("%8s", Integer.toBinaryString(bg & 0xFF)).replace(' ', '0');
			String sb = String.format("%8s", Integer.toBinaryString(bb & 0xFF)).replace(' ', '0');
			bitEncode+=sr+sg+sb+"\n";
		}
		System.out.println("DONE!");
		return bitEncode;
		
	}
	
	public void decodeImage(String encode, String fileDir){
		System.out.println("Decoding Image");
		BufferedImage newImage;
		if(encode.contains("\n")){
			String[] pixels = encode.split("\n");
			int imageSize = (int) Math.sqrt(pixels.length);
			System.out.println("Image Size is: "+imageSize);
			newImage = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_RGB);
			int q = 0;
			for(int i = 0; i<imageSize;i++){
				for(int j =0; j<imageSize;j++){
					newImage.setRGB(j, i, Integer.parseInt(pixels[q], 2));
					q++;
				}
			}
			
			File outputfile = new File(fileDir);
			System.out.println("Saving Image");
			try {
				ImageIO.write(newImage, "jpg", outputfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("DONE!");
		}
	}
	
	public String getGeneFormat(String encode){
		return encode.replaceAll("\n", "");
	}
	
	public String getEncodingFormate(String gene){
		String newString = gene.replaceAll("(.{24})(?!$)", "$1\n");
		return newString;
	}
	
	public void writeBinaryToFile(String filedir, String content){
		 BufferedWriter bufferedWriter = null;
	        try {
	            String strContent = content;
	            File myFile = new File(filedir);
	            // check if file exist, otherwise create the file before writing
	            if (!myFile.exists()) {
	                myFile.createNewFile();
	            }
	            Writer writer = new FileWriter(myFile);
	            bufferedWriter = new BufferedWriter(writer);
	            bufferedWriter.write(strContent);
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally{
	            try{
	                if(bufferedWriter != null) bufferedWriter.close();
	            } catch(Exception ex){
	                 
	            }
	        }
	}
}
