
public class Population {
	Individual[] pop;
	public Population(int popSize, boolean initialize){
		pop = new Individual[popSize];
		if(initialize){
			for(int i =0; i<getPopSize(); i++){
				Individual newI = new Individual();
				newI.generateIndividual();
				saveIndividual(i,newI);
			}
		}
	}
	
	public Individual getIndividual(int i){
		return pop[i];
	}
	
	public Individual getFittest(){
		Individual fittest = pop[0];
		
		for(int i =0; i< getPopSize(); i++){
			if(fittest.getFitness()<= getIndividual(i).getFitness()){
				fittest = getIndividual(i);
			}
		}
		return fittest;
	}
	
	
	 public int getPopSize() {
		 return pop.length;
	 }
	 
	 public void saveIndividual(int i, Individual indiv) {
	        pop[i] = indiv;
	    }
}
