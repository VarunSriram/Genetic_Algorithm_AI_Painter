
public class Individual {
	static int defaultGeneLength = 64;
	private byte[] genes = new byte[defaultGeneLength];
	
	private int fitness = 0;
	
	
	
	public void generateIndividual(){
		for(int i =0; i<getSize(); i++){
			byte gene = (byte) Math.round(Math.random());
			genes[i] = gene;
		}
	}
	
	public static void setDefaultGeneLength(int length) {
        defaultGeneLength = length;
    }
	
	public byte getGene(int i){
		return genes[i];
	}
	
	public void setGene(int i, byte v){
		genes[i] = v;
		fitness = 0;
	}
	
	public int getSize(){
		return genes.length;
	}
	
	public int getFitness(){
		if(fitness == 0){
			fitness  = FitnessCalculator.getFitness(this);
			
		}
		return fitness;
		
	}
	
	@Override
	public String toString(){
		String geneString = "";
		for(int i = 0; i<getSize();i++){
			geneString += getGene(i);
		}
		return geneString;
	}
	
	
	
}
