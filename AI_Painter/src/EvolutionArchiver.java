import java.util.ArrayList;
import java.util.HashMap;

public class EvolutionArchiver {
	private HashMap<Integer, ArrayList<String>> evo = new HashMap<Integer, ArrayList<String>>();
	
	public void addEntry(int pixel, String encode){
		if(evo.containsKey(pixel)){
			evo.get(pixel).add(encode);
		}
		
		else{
			evo.put(pixel, new ArrayList<String>());
			evo.get(pixel).add(encode);
		}
	}
	
	public int getEntrySize(int pixel){
		return evo.get(pixel).size();
	}
	
	public int padData(){
		int maxSize = this.findLargestDataSet();
		for(Integer key : evo.keySet()){
			while(evo.get(key).size() != maxSize){
				String last = evo.get(key).get(evo.get(key).size()-1);
				evo.get(key).add(last);
			}
		}
		return maxSize;
	}
	
	public int findLargestDataSet(){
		int champ = -10000;
		for(Integer key : evo.keySet()){
			if(evo.get(key).size()> champ){
				champ = evo.get(key).size();
			}
		}
		
		return champ;
	}
	
	public String getGeneration(int gen){
		String x = "";
		for(Integer key : evo.keySet()){
			x+= evo.get(key).get(gen);
		}
		return x;
	}
	
	
	public void printData(int i){
		for(String s : evo.get(i)){
			System.out.println(s);
		}
	}
	
	
	
	
	
	
}
