
public class TotalImage extends RunGene {

	public TotalImage(String gene) {
		super(gene);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void runGA() {
		// TODO Auto-generated method stub
		Individual.setDefaultGeneLength(gene.length());
	 	FitnessCalculator.setSolution(gene);

        // Create an initial population
        Population myPop = new Population(500, true);
       // ((float) myPop.getFittest().getFitness())/((float)gene.length()) < 0.95f
        // Evolve our population until we reach an optimum solution
        int generationCount = 0;
        while (myPop.getFittest().getFitness() < FitnessCalculator.getMaxFitness() && ((float) myPop.getFittest().getFitness())/((float)gene.length()) < 0.90f) {
            generationCount++;
            System.out.println("Generation: " + generationCount + " Fittest: " + myPop.getFittest().getFitness());
            System.out.println( ((float) myPop.getFittest().getFitness())/((float)gene.length()));
            if(generationCount%10 == 0){
            	System.out.println("ImageSource/fitness.jpg");
            String x = ImageEncoder.getInstance().getEncodingFormate(myPop.getFittest().toString());
            ImageEncoder.getInstance().decodeImage(x, "ImageSource/"+generationCount+".jpg");
            }
            myPop = GeneticAlgorithm.evolvePopulation(myPop);
        }
        System.out.println("Solution found!");
        System.out.println("Generation: " + generationCount);
        System.out.println("Genes:");
        System.out.println(myPop.getFittest());
        String x = ImageEncoder.getInstance().getEncodingFormate(myPop.getFittest().toString());
        ImageEncoder.getInstance().decodeImage(x, "ImageSource/"+generationCount+"final.jpg");
	}
	
	

}
