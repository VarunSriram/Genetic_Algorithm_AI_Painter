
public abstract  class RunGene {
	protected String gene;
	public RunGene(String gene){
		this.gene = gene;
	}
	
	public abstract void runGA();
}
